<?php

// We need to use sessions, so you should always start sessions using the below code.

session_start();

// If the user is not logged in redirect to the login page...

if (!isset($_SESSION['loggedin'])) {

	header('Location: index.html');

	exit();

}

?>

<!DOCTYPE html>

<html lang="en">

<head>

    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="../../res/css/dashboard.css">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">

    <title>Document</title>

</head>

<body>





<?php require_once "../../includes/header.php"; ?>



<div class="content">



<div class="tagHolder">

<ul id="tags">



<?php

    $query = 'SELECT * FROM tags ORDER BY tag_title DESC;';

    $path = $_SERVER['DOCUMENT_ROOT'];

    $path .= "/classes/dbh.php";

    require_once($path);

    $dbh = new Dbh();

    $rows = $dbh->executeSelect($query);

?>



                <?php foreach ($rows as $row) { ?><li><?php echo ucfirst($row['tag_title']); ?></li><?php } ?>



                    </ul>

                </div>

            <form method="POST" action="/adminpanel/dashboard/actions/tags/addTag.php">

                <input type="text" name="tag" placeholder="Enter a tag" required>

                <input type="submit" value="Add Tag">

            </form>

            <form method="POST" action="/adminpanel/dashboard/actions/tags/deleteTag.php">

                <input type="text" name="tag" placeholder="Enter a tag" required>

                <input type="submit" value="Delete Tag">

            </form>

        </div>

    </body>

</html>