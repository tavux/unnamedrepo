<?php

// We need to use sessions, so you should always start sessions using the below code.

session_start();

// If the user is not logged in redirect to the login page...

if (!isset($_SESSION['loggedin'])) {

	header('Location: index.html');

	exit();

}

?>

<!DOCTYPE html>

<html lang="en">



<head>

    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <script src="https://code.jquery.com/jquery-3.4.1.min.js"

        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="../../res/css/dashboard.css">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"

        integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">





    <!--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">-->

    <title>Document</title>

</head>



<body>





    <?php require_once "../../includes/header.php"; ?>



    <div class="content">





        <form action="/adminpanel/dashboard/actions/entries/create.php" method="post">

            <h1>Author</h1>

            <input type="text" name="entry_author" placeholder="Author" id="author" required>







            <h1>Title</h1>

            <input type="text" name="entry_title" placeholder="English" id="title" required>

            <input type="text" name="entry_title_sv" placeholder="Swedish" id="title_sv" required>

            <input type="text" name="entry_title_fi" placeholder="Finnish" id="title_fi" required>

            <h1 style="display: inline-block;">Category</h1>

            <p style="display: inline-block;"> - Will get auto translated</p>

            <select name="entry_category" id="category" required>

                <option selected disabled>Select category</option>

                <option value="Academic">Academic</option>

                <option value="Integrative">Integrative</option>

                <option value="Alternative">Alternative</option>

                <option value="Experiences">Experiences</option>

                <option value="News">News</option>

            </select>





            <h1>Content</h1>





            <div class="toolbar">

                <a href="javascript:void()" data-command='bold'><i class='fa fa-bold'></i></a>

                <a href="javascript:void()" data-command='italic'><i class='fa fa-italic'></i></a>

                <a href="javascript:void()" data-command='strikeThrough'><i class='fa fa-strikethrough'></i>

                    <a href="javascript:void()" data-command='justifyLeft'><i class='fa fa-align-left'></i></a>

                    <a href="javascript:void()" data-command='justifyCenter'><i class='fa fa-align-center'></i></a>

                    <a href="javascript:void()" data-command='justifyRight'><i class='fa fa-align-right'></i></a>

                    <a href="javascript:void()" data-command='justifyFull'><i class='fa fa-align-justify'></i></a>

                    <a href="javascript:void()" data-command='insertUnorderedList'><i class='fa fa-list-ul'></i></a>

                    <a href="javascript:void()" data-command='insertOrderedList'><i class='fa fa-list-ol'></i></a>

                    <a href="javascript:void()" data-command='h1'>H1</a>

                    <a href="javascript:void()" data-command='h2'>H2</a>

                    <a href="javascript:void()" data-command='createlink'><i class='fa fa-link'></i></a>

                    <a href="javascript:void()" data-command='insertimage'><i class='fa fa-image'></i></a>

                    <a href="javascript:void()" data-command='p'>P</a>

            </div>

            <p>Article English</p>

            <div id='editor' contenteditable></div>

            <p style="margin-top: 30px;">Article Swedish</p>

            <div id='editor_se' contenteditable></div>

            <p style="margin-top: 30px;">Article Finnish</p>

            <div id='editor_fi' contenteditable></div>





            <textarea placeholder="English source" id="hidden_text_en" name="entry_content" required></textarea>

            <textarea placeholder="Finnish source" id="hidden_text_se" name="entry_content_sv" required></textarea>

            <textarea placeholder="Finnish source" id="hidden_text_fi" name="entry_content_fi" required></textarea>

            <br><br>

            <h1>Excerpt</h1><input id="autoExc" type="button" value="AUTO">

            <textarea placeholder="English excerpt" name="entry_excerpt" id="textexcerpt" cols="30" rows="10" required

                contenteditable></textarea>

            <textarea placeholder="Swedish excerpt" name="entry_excerpt_sv" id="textexcerpt_sv" cols="30" rows="10"

                required contenteditable></textarea>

            <textarea placeholder="Finnish excerpt" name="entry_excerpt_fi" id="textexcerpt_fi" cols="30" rows="10"

                required contenteditable></textarea>

            <br><br>

            <p>Publication date</p>

            <input type="text" name="entry_date" id="entry_date" required><input type="button" value="SET NOW"

                id="dateNow_">

            <br><br>

            <input type="text" name="entry_image_id" id="entry_img_id" />

            <br><br>

            <p>Select tags - Will get auto translated</p>

            <ul id="tagSelector">



                <?php

    $query = 'SELECT * FROM tags ORDER BY tag_title ASC;';

    $path = $_SERVER['DOCUMENT_ROOT'];

    $path .= "/classes/dbh.php";

    require_once($path);

    $dbh = new Dbh();

    $rows = $dbh->executeSelect($query);

    

    foreach ($rows as $row) { ?>

                <li>

                    <input type="checkbox" name="tag_list[]" value="<?php echo $row['tag_id']; ?>">

                    <?php echo $row['tag_title'];?>

                </li>



                <?php } ?>



            </ul>

            <br>

            <br>

            <input type="submit" value="Add post">

            <input type="button" id="save" value="CONVERT TO HTML">

        </form>

    </div>



    <script>

    $(function() {

        $('#save').click(function() {

            var mysave = $('#editor').html();

            var mysave_se = $('#editor_se').html();

            var mysave_fi = $('#editor_fi').html();

            var str;

            var str_se;

            var str_fi;



            str = mysave.replace(/(style="([^>]+)")/ig, "");

            str = str.replace(/(class="([^>]+)")/ig, "");

            str = str.replace(/(<span([^>]+)>)/ig, "");

            str = str.replace(/(<([^>]+)span>)/ig, "");

            str = str.replace(/(<o:p>)/ig, "");

            str = str.replace(/(<([^>]+)o:p>)/ig, "");

            str = str.replace(/(<p([^>]+)>)/ig, "<p>");

            str = str.replace(/(<h2([^>]+)>)/ig, "<h2 class=\"subtitle\">");

            str = str.replace(/(<em>)/ig, "");

            str = str.replace(/(<b>)/ig, "");

            str = str.replace(/(<([^>]+)em>)/ig, "");

            str = str.replace(/(<([^>]+)b>)/ig, "");

            $('#hidden_text_en').val(str);



            str_se = mysave_se.replace(/(style="([^>]+)")/ig, "");

            str_se = str_se.replace(/(class="([^>]+)")/ig, "");

            str_se = str_se.replace(/(<span([^>]+)>)/ig, "");

            str_se = str_se.replace(/(<([^>]+)span>)/ig, "");

            str_se = str_se.replace(/(<o:p>)/ig, "");

            str_se = str_se.replace(/(<([^>]+)o:p>)/ig, "");

            str_se = str_se.replace(/(<p([^>]+)>)/ig, "<p>");

            str_se = str_se.replace(/(<h2([^>]+)>)/ig, "<h2 class=\"subtitle\">");

            str_se = str_se.replace(/(<em>)/ig, "");

            str_se = str_se.replace(/(<b>)/ig, "");

            str_se = str_se.replace(/(<([^>]+)em>)/ig, "");

            str_se = str_se.replace(/(<([^>]+)b>)/ig, "");

            $('#hidden_text_se').val(str_se);



            str_fi = mysave_fi.replace(/(style="([^>]+)")/ig, "");

            str_fi = str_fi.replace(/(class="([^>]+)")/ig, "");

            str_fi = str_fi.replace(/(<span([^>]+)>)/ig, "");

            str_fi = str_fi.replace(/(<([^>]+)span>)/ig, "");

            str_fi = str_fi.replace(/(<o:p>)/ig, "");

            str_fi = str_fi.replace(/(<([^>]+)o:p>)/ig, "");

            str_fi = str_fi.replace(/(<p([^>]+)>)/ig, "<p>");

            str_fi = str_fi.replace(/(<h2([^>]+)>)/ig, "<h2 class=\"subtitle\">");

            str_fi = str_fi.replace(/(<em>)/ig, "");

            str_fi = str_fi.replace(/(<b>)/ig, "");

            str_fi = str_fi.replace(/(<([^>]+)em>)/ig, "");

            str_fi = str_fi.replace(/(<([^>]+)b>)/ig, "");

            $('#hidden_text_fi').val(str_fi);

        });

    });





    document.getElementById("dateNow_").addEventListener("click", function() {

        //var date = new Date().toLocaleString()

        if (this.value === "SET NOW") {

            document.getElementById("entry_date").value = "NOW";

            document.getElementById("entry_date").innerHTML = "NOW";

            document.getElementById("entry_date").readonly = true;

            document.getElementById("entry_date").style.opacity = "0.3";

            this.style.background = "rgba(220,120,20,0.8)";

            this.value = "EDIT";

        } else {

            document.getElementById("entry_date").value = "";

            document.getElementById("entry_date").innerHTML = "";

            document.getElementById("entry_date").readonly = false;

            document.getElementById("entry_date ").style.opacity = "1";

            this.style.background = "rgba(255,255,255,1)";

            this.value = "SET NOW";



        }

    });



    $('.toolbar a').click(function(e) {

        var command = $(this).data('command');

        if (command == 'h1' || command == 'h2' || command == 'p') {

            document.execCommand('formatBlock', false, command);

        }

        if (command == 'forecolor' || command == 'backcolor') {

            document.execCommand($(this).data('command'), false, $(this).data('value'));

        }

        if (command == 'createlink' || command == 'insertimage') {

            url = prompt('Enter the link here: ', 'http:\/\/');

            document.execCommand($(this).data('command'), false, url);

        } else document.execCommand($(this).data('command'), false, null);

    });

    </script>







    <div class="footer"></div>

</body>



</html>