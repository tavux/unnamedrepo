<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../res/css/dashboard.css">
    <title>Document</title>
</head>
<body>
    
<?php require_once "../../includes/header.php"; ?>

<div class="content">

<?php
$path = $_SERVER['DOCUMENT_ROOT'];
$path .= "/dist/classes/entry.php";
include_once($path);

$entry = new Entry();




?>

<?php 
    $query = 'SELECT * FROM entries ORDER BY entry_date DESC;';


    $path = $_SERVER['DOCUMENT_ROOT'];
    $path .= "/dist/classes/dbh.php";
    require_once($path);

    $dbh = new Dbh();
    $rows = $dbh->executeSelect($query);
    ?>

<?php
    foreach ($rows as $row) {
    $entry = new Entry();
    $entry->setByRow($row);
    ?>


<!-- Structure of article -->
<div class="postWrapper">
    <a href="../entries/delete.php?id=<?php echo $entry->getId(); ?>" class="delBtn">X</a>
    <p><a href="single.php?entry_id=<?php echo $entry->getId(); ?>"><?php echo $entry->getTitle("en"); ?></a><br>
    <?php echo $entry->getExcerpt("en"); ?></p>
</div>
<?php } ?>

</div>

</body>
</html>