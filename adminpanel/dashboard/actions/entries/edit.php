<?php
// We need to use sessions, so you should always start sessions using the below code.
session_start();
// If the user is not logged in redirect to the login page...
if (!isset($_SESSION['loggedin'])) {
	header('Location: index.html');
	exit();
}
$path = $_SERVER['DOCUMENT_ROOT'];
$path .= "/classes/entry.php";
require_once($path);

$entry = new Entry();

$entry->SqlSelectEntryById($_GET['id']);


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../res/css/dashboard.css">
    <title>Document</title>
</head>

<body>


    <?php require_once "../../includes/header.php"; ?>



    <div class="content">


        <form action="/adminpanel/dashboard/actions/entries/create.php" method="post">
            <h1>Author</h1>
            <input type="text" name="entry_author" placeholder="Author" id="author" value="<?php echo $entry->getAuthor();?>"  required>



            <h1>Title</h1>
            <input type="text" name="entry_title" placeholder="English" id="title" value="<?php echo $entry->getTitle("en");?>" required>
            <input type="text" name="entry_title_sv" placeholder="Swedish" id="title_sv" value="<?php echo $entry->getTitle("sv");?>" required>
            <input type="text" name="entry_title_fi" placeholder="Finnish" id="title_fi" value="<?php echo $entry->getTitle("fi");?>" required>
            <h1 style="display: inline-block;">Category</h1>
            <p style="display: inline-block;"> - Will get auto translated</p>
            <select name="entry_category" id="category" required>
                <option selected disabled>Select category</option>
                <option value="Academic">Academic</option>
                <option value="Integrative">Integrative</option>
                <option value="Alternative">Alternative</option>
                <option value="Experiences">Experiences</option>
                <option value="News">News</option>
            </select>
            <script>
            var str = "<?php echo $entry->getCategory(); ?>";
            SelectElement("category", str);

            function SelectElement(id, valueToSelect)
            {    
                var element = document.getElementById(id);
                element.value = valueToSelect;
            }
            </script>

            <h1>Content</h1>


            <textarea placeholder="English source" id="hidden_text_en" name="entry_content" required><?php echo $entry->getContent("en"); ?></textarea>
            <textarea placeholder="Finnish source" id="hidden_text_se" name="entry_content_sv" required><?php echo $entry->getContent("sv"); ?></textarea>
            <textarea placeholder="Finnish source" id="hidden_text_fi" name="entry_content_fi" required><?php echo $entry->getContent("fi"); ?></textarea>
            <br><br>
            <h1>Excerpt</h1>
            <textarea placeholder="English excerpt" name="entry_excerpt" id="textexcerpt" cols="30" rows="10" required
                contenteditable><?php echo $entry->getExcerpt("en"); ?></textarea>
            <textarea placeholder="Swedish excerpt" name="entry_excerpt_sv" id="textexcerpt_sv" cols="30" rows="10"
                required contenteditable><?php echo $entry->getExcerpt("sv"); ?></textarea>
            <textarea placeholder="Finnish excerpt" name="entry_excerpt_fi" id="textexcerpt_fi" cols="30" rows="10"
                required contenteditable><?php echo $entry->getExcerpt("fi"); ?></textarea>
            <br><br>
            <p>Publication date</p>
            <input type="text" name="entry_date" id="entry_date" value="<?php echo $entry->getDate(); ?>" required><input type="button" value="SET NOW"
                id="dateNow_">
            <br><br>
            <input type="text" name="entry_image_id" id="entry_img_id" />
            <br><br>
            <p>Select tags - Will get auto translated</p>
            <ul id="tagSelector">

                <?php
$query = 'SELECT * FROM tags ORDER BY tag_title ASC;';
$path = $_SERVER['DOCUMENT_ROOT'];
$path .= "/classes/dbh.php";
require_once($path);
$dbh = new Dbh();
$rows = $dbh->executeSelect($query);

foreach ($rows as $row) { ?>
                <li>
                    <input type="checkbox" name="tag_list[]" value="<?php echo $row['tag_id']; ?>">
                    <?php echo $row['tag_title'];?>
                </li>

                <?php } ?>

            </ul>
            <br>
            <br>
            <input type="submit" value="Add post">
            <input type="button" id="save" value="TEST">
        </form>
    </div>

    <script>
    document.getElementById("dateNow_").addEventListener("click", function() {
        //var date = new Date().toLocaleString()
        if (this.value === "SET NOW") {
            document.getElementById("entry_date").value = "NOW";
            document.getElementById("entry_date").innerHTML = "NOW";
            document.getElementById("entry_date").readonly = true;
            document.getElementById("entry_date").style.opacity = "0.3";
            this.style.background = "rgba(220,120,20,0.8)";
            this.value = "EDIT";
        } else {
            document.getElementById("entry_date").value = "";
            document.getElementById("entry_date").innerHTML = "";
            document.getElementById("entry_date").readonly = false;
            document.getElementById("entry_date ").style.opacity = "1";
            this.style.background = "rgba(255,255,255,1)";
            this.value = "SET NOW";
        }
    });
    </script>





</body>

</html>