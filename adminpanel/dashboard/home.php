<?php

// We need to use sessions, so you should always start sessions using the below code.

session_start();

// If the user is not logged in redirect to the login page...

if (!isset($_SESSION['loggedin'])) {

	header('Location: /Petrafoundation/adminpanel/index.html');

	exit();

}

?>



<!DOCTYPE html>

<html>

	<head>

		<meta charset="utf-8">

		<title>Home Page</title>

		<link href="res/css/dashboard.css" rel="stylesheet" type="text/css">

		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">

	</head>

	<body class="loggedin">



	<?php require_once "includes/header.php"; ?>







        <div class="content">

            <h2>Welcome back, <?=$_SESSION['name']?>!</h2>

		



			<h3>Post management</h3>

            <p><a class="action" href="actions/entries/add_post.php">Add posts</a></p>

            <p><a class="action" href="actions/entries/edit_post.php">Edit posts</a></p>

            <p><a class="action" href="actions/entries/delete_post.php">Delete posts</a></p>

        </div>

        <div class="content">



			<h3>Tag management</h3>

            <p><a class="action" href="actions/tags/tag_management.php">Manage Tags</a></p>

        </div>



		<?php 

		if(isset($_GET['action'])) {



			require_once "includes/footer.php"; 

			echo "Article has been  ";

			echo $_GET['action'];

			echo "!";

			echo "</div>";

		}

		?>

		<div class="content">

			<h3>Questionaire management</h3>



			<?php

			

			$root = realpath($_SERVER["DOCUMENT_ROOT"]);

			require_once "$root/res/config.php";

			

			if($questionaireEnabled) {

				?>

			<input type="button" value="DISABLE QUESTIONAIRE">

			<?php } ?>



		</div>

	</body>

</html>