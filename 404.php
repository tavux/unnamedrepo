<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
	<title>Not found</title>
	<style>
		body {
			background: #70afda;
			font-family: 'Montserrat', sans-serif;
			text-align: center;
		}
		h1 {
			position: absolute;
			top: 50%;
			left: 50%;
			transform:translate(-50%,-50%);
			color: white;
		}
	</style>
</head>
<body>

<h1><?php echo "The requested page was not found, you'll be redirected in 5 seconds..."; ?></h1>


<?php 
header("Refresh:5; url=index.php");
exit;
?>
</body>
</html>
