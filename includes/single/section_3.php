<?php
// SHOULD BE ARTICLES WITH SAME TAGS
$currentEntry = preg_replace( '/[^0-9]/', '', $_GET['entry_id']);

$cat__ = $entry->getCategory($lang);

$cat_ = explode(", ", $cat__);

$len = sizeof($cat_);
switch ($len) {
    case "1":
        $query = "SELECT * FROM entries WHERE (`entry_category` LIKE '%$cat_[0]%') AND (`entry_id` <>$currentEntry) ORDER BY entry_views and entry_date DESC LIMIT 3;";
        break;
    case "2":
        $query = "SELECT * FROM entries WHERE (`entry_category` LIKE '%$cat_[0]%' OR `entry_category` LIKE '%$cat_[1]%') AND (`entry_id` <>$currentEntry) ORDER BY entry_views and entry_date DESC LIMIT 3;";
        break;
    case "3":
        $query = "SELECT * FROM entries WHERE (`entry_category` LIKE '%$cat_[0]%' OR `entry_category` LIKE '%$cat_[1]%' OR `entry_category` LIKE '%$cat_[2]%') AND (`entry_id` <>$currentEntry) ORDER BY entry_views and entry_date DESC LIMIT 3;";
        break;
    case "4":
        $query = "SELECT * FROM entries WHERE (`entry_category` LIKE '%$cat_[0]%' OR `entry_category` LIKE '%$cat_[1]%' OR `entry_category` LIKE '%$cat_[2]%' OR `entry_category` LIKE '%$cat_[3]%') AND (`entry_id` <>$currentEntry) ORDER BY entry_views and entry_date DESC LIMIT 3;";
        break;
    case "5":
        $query = "SELECT * FROM entries WHERE (`entry_category` LIKE '%$cat_[0]%' OR `entry_category` LIKE '%$cat_[1]%' OR `entry_category` LIKE '%$cat_[2]%' OR `entry_category` LIKE '%$cat_[3]%' OR `entry_category` LIKE '%$cat_[4]%') AND (`entry_id` <>$currentEntry) ORDER BY entry_views and entry_date DESC LIMIT 3;";
        break;
    default:
        $query = "SELECT * FROM entries WHERE (`entry_category` LIKE '%$cat_[0]%') AND (`entry_id` <>$currentEntry) ORDER BY entry_views and entry_date DESC LIMIT 3;";
        break;
}


$dbh = new Dbh();
$rows = $dbh->executeSelect($query);
if($rows) {
$related_articles_title;
    switch ($lang) {
        case "en":
            $related_articles_title = "Related articles";
            break;
        case "sv":
            $related_articles_title = "Relaterade artiklar";
            break;
        case "fi":
            $related_articles_title = "Aiheeseen liittyät artikkelit"; 
            break;

        default:
            $related_articles_title = "";
            break; 
    }

    
$imageFormat;

if (strpos($_SERVER['HTTP_ACCEPT'], 'image/webp') !== false) {
    // webp is supported!
    $imageFormat = "webp";
} else {
    $imageFormat = "jpg";
}




?>

<h2 id="related_articles_title"><u><?php echo $related_articles_title; ?></u></h2>
<div class='some-page-wrapper'>
    <div class='row'>

        <?php


foreach ($rows as $row) {

    $entry = new Entry();
    $entry->setByRow($row);

?>

        <div class="column <?php echo $entry->getCategory(); ?>">
        <section class="top" style="background: linear-gradient(rgba(200,200,200, 0.45), rgba(200,200,200, 0.45)), url('/res/images/articles/thumb/<?php echo $imageFormat; ?>/<?php echo $entry->getImgId();?>_thumb.<?php echo $imageFormat; ?>');background-position:center;background-size:cover;background-repeat:no-repeat;">

        "></section>
            <section class="bottom"> <a href="post.php?lang=<?php echo $lang;?>&entry_id=<?php echo $entry->getId(); ?>"
                    class="title"><?php echo $entry->getTitle($lang); ?></a>
                <p class="excerpt"><?php echo $entry->getExcerpt($lang); ?></p>
            </section>
        </div>

        <?php } ?>

    </div>
</div>
<?php }?>
<script>
$('.some-page-wrapper').children('.row').children('.column').each(function () {
    $(this).on("click", function() {
        var go_to_location = $(this).children('.bottom').children('a').attr('href');
        window.location.href = go_to_location;
    });
});
</script>