<div id="wrapper_article">



    <?php if ($entry->getId() != -1) { ?>

    <!-- Post -->



    <h1 class="post_title"><u><?php echo $entry->getTitle($lang); ?></u></h1>



    <article class="post">



        <div id="img" style="background: url('/res/images/articles/<?php echo $entry->getImgId(); ?>.jpg') no-repeat center;background-size:cover;"></div>

        <div class="contentContainer">

            <time class="post_published">

                

                <?php

                $published = date_create($entry->getDate());

                $dateString;



                switch ($lang) {

                    case "en":

                        $dateString = date('M jS, Y', strtotime($entry->getDate())); 

                        break;

                    case "sv":

                        $dateString = date('d.m.Y', strtotime($entry->getDate())); 

                        break;

                    case "fi":

                    $dateString = date('d.m.Y', strtotime($entry->getDate())); 

                        break;



                    default:

                        $dateString = "";

                        break; 

                }



                echo $dateString;



                ?>

            </time>

            <div class="post_content"><?php echo $entry->getContent($lang); ?></div>

        </div>

<!--<time class="post_edited">Edited <?php //$published=date_create($entry->getDate()); echo date_format($published,"d.m.Y - H:i");?></time>-->

        <br>

        <!--<a href="#" class="author"><span class="name"><?php echo $entry->getAuthor(); ?></span></a>-->



        <div class="shareKnowledge">



<?php

switch ($lang) {

    case "en":

        $share_message = "Share the knowledge!";

        break;

    case "sv":

        $share_message = "Dela kunskapen!";

        break;

    case "fi":

        $share_message = "Jaa tieto!";

        break;

    default:

    $share_message = "Share the knowledge!";

}

?>



            <p class="shareTheKnowledge"><?php echo $share_message;?></p>

            <div id="share_ul" class="a2a_kit a2a_kit_size_32 a2a_default_style">

                <a class="a2a_button_facebook"></a>

                <a class="a2a_button_twitter"></a>

                <a class="a2a_button_email"></a>

            </div>

            <script async src="https://static.addtoany.com/menu/page.js"></script>

            <!-- AddToAny END -->

        </div>

    </article>



    <script>

    document.getElementById("printThisPage").addEventListener("click", function() {

        window.print();

    });

    </script>





    <?php } else { ?>

    <!-- Post -->

    <article class="post">

        <header>

            <div class="title">

                <h2><a href="#">404 Entry not found.</a></h2>

            </div>

            <div class="meta">

                <time class="published"></time>

                <a href="#" class="author"><span class="name"></span></a>

            </div>

        </header>

        Sorry :( The entry you are looking for has been removed or doesn't exist.

    </article>



    <?php } ?>





</div>