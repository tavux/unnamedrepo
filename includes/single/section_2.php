<?php

        $t_title = "";

        switch ($lang) {
            case "en":
                $t_title = "tag_title";
                break;
            case "sv":
                $t_title = "tag_title_sv";
                break;
            case "fi":
                $t_title = "tag_title_fi";
                break;
            default:
                $t_title = "tag_title";
        }


      $id = $entry->getId();
        
      $query = "SELECT t.tag_title, t.tag_title_sv, t.tag_title_fi
      FROM tags t
      INNER JOIN entries_tags et on t.tag_id = et.tag_id
      INNER JOIN entries e on et.entries_id = e.entry_id
      WHERE e.entry_id = '".$id."' ORDER BY '".$t_title."' DESC LIMIT 50";

      $dbh = new Dbh();
      $rows = $dbh->executeSelect($query);

      if($rows) {
        $related_tag_title;
        $tag_lang = "_" . $lang;
        if($lang === "en") {
            $tag_lang = "";
        }
                    
            switch ($lang) {
                case "en":
                    $related_tag_title = "Related tags";
                    break;
                case "sv":
                    $related_tag_title = "Relaterade taggar";
                    break;
                case "fi":
                    $related_tag_title = "Aiheeseen liittyät avainsanat"; 
                    break;
        
                default:
                    $related_tag_title = "";
                    break; 
            }
?>


<h2 id="related_tags_title"><u><?php echo $related_tag_title; ?></u></h2>
<ul id="tags">
    <?php 

        foreach ($rows as $row) {
            echo "<li><a href='/pages/search.php?lang=$lang&tag=" . $row['tag_title' . $tag_lang] ."'>" . ucfirst($row[$t_title]) . "</a></li>";
        }
    ?>
</ul>
    <?php } ?>