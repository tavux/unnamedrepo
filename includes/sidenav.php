<?php



$academic_sidenav = "ACADEMIC";

$integrative_sidenav = "INTEGRATIVE";

$alternative_sidenav = "ALTERNATIVE";

$experiences_sidenav = "EXPERIENCES";



if($lang === 'en') {

    $academic_sidenav = "ACADEMIC";

    $integrative_sidenav = "INTEGRATIVE";

    $alternative_sidenav = "ALTERNATIVE";

    $experiences_sidenav = "EXPERIENCES";

    $menuBtn = "MENU";

    $closeBtn = "CLOSE";

    } else if($lang === 'sv') {

        $academic_sidenav = "AKADEMISK";

        $integrative_sidenav = "INTEGRATIV";

        $alternative_sidenav = "ALTERNATIV";

        $experiences_sidenav = "ERFARENHETER";

        $menuBtn = "MENY";

        $closeBtn = "STÄNG";



    } else if($lang === 'fi') {

        $academic_sidenav = "AKATEEMINEN";

        $integrative_sidenav = "INTEGRATIIVINEN";

        $alternative_sidenav = "VAIHTOEHTOINEN";

        $experiences_sidenav = "KOKEMUKSIA";

        $menuBtn = "VALIKKO";

        $closeBtn = "SULJE";



    }



?>





<!-- Navigatonbar on the side-->



<input id="menuBtn" type="button" value="<?php echo $menuBtn; ?>">

<div id="sideNav_wrapper">

    <ul id="sideNav">

        <li><a href="/pages/categories.php?lang=<?php echo $lang;?>&category=academic"><?php echo $academic_sidenav ?></a></li>

        <li><a href="/pages/categories.php?lang=<?php echo $lang;?>&category=integrative"><?php echo $integrative_sidenav ?></a></li>

        <li><a href="/pages/categories.php?lang=<?php echo $lang;?>&category=alternative"><?php echo $alternative_sidenav ?></a></li>

        <li><a href="/pages/categories.php?lang=<?php echo $lang;?>&category=experiences"><?php echo $experiences_sidenav ?></a></li>

    </ul>



</div>





<script>

document.getElementById("menuBtn").addEventListener("click", function() {



            this.classList.toggle("opened")



            if(this.classList.contains("opened")) {

                document.getElementById("navigation_top_wrapper").style.display = "block";

                document.getElementById("sideNav").style.visibility = "hidden";

                document.getElementById("langSelector").style.top = "80vh";

                this.value = "<?php echo $closeBtn; ?>";

            } else {

                document.getElementById("navigation_top_wrapper").style.display = "none";

                document.getElementById("sideNav").style.visibility = "visible";

                document.getElementById("langSelector").style.top = "-80vh";

                this.value = "<?php echo $menuBtn; ?>";

            }

        });

        </script>