<?php

$imageFormat;

if (strpos($_SERVER['HTTP_ACCEPT'], 'image/webp') !== false) {
    // webp is supported!
    $imageFormat = "webp";
} else {
    $imageFormat = "jpg";
}

?>

<div class='row'>
    <div class='column'>
        <div class='blue-column'>
            <div class="fb-page" data-href="https://www.facebook.com/petrafoundation" data-tabs="timeline" data-height="1190" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false">
                <blockquote cite="https://www.facebook.com/petrafoundation" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/petrafoundation">Petra Flander stiftelse för integrerad
                        vård r.s.
                        /
                        Petrafoundation</a></blockquote>
            </div>
        </div>
    </div>
    <div class='column'>
        <div class='green-column'>

            <?php

            switch ($lang) {
                case "en":
                    $ourTopArticlesTitle = 'Our top articles';

                    break;
                case "sv":
                    $ourTopArticlesTitle = 'Våra bästa artiklar';

                    break;
                case "fi":
                    $ourTopArticlesTitle = 'Tärkeimmät artikkelimme';

                    break;
                default:
                    $ourTopArticlesTitle = 'Our top articles';
                    break;
            }
            ?>

                <h1 class="top_article_title"><u><?php echo $ourTopArticlesTitle; ?></u></h1>

                <?php

                $query = "SELECT * FROM entries ORDER BY entry_views and entry_date DESC LIMIT 3;";

                $dbh = new Dbh();
                $rows = $dbh->executeSelect($query);

                foreach ($rows as $row) {

                    $entry = new Entry();
                    $entry->setByRow($row);

                    ?>

                    <div class="top_article">
                        <section class="top" style="
                        background: #CACACA url('/res/images/articles/thumb/<?php echo $imageFormat; ?>/<?php echo $entry->getImgId(); ?>_thumb.<?php echo $imageFormat; ?>');
                background-size: cover;
                background-position: center;
                background-repeat: no-repeat;"></section>
                        <section class="bottom">
                            <time class="published"><?php

                                                        echo date('M jS, Y', strtotime($entry->getDate()));

                                                        //echo $entry->getDate();


                                                        ?></time>
                            <a href="pages/post.php?lang=<?php echo $lang; ?>&entry_id=<?php echo $entry->getId(); ?>" class="title"><?php echo $entry->getTitle($lang); ?></a>
                            <p class="excerpt"><?php echo $entry->getExcerpt($lang); ?></p>
                            <p class="dot">...</p>
                        </section>
                    </div>
                <?php } ?>
        </div>
    </div>
</div>
<script>
    $('.green-column').children('.top_article').each(function() {
        $(this).on("click", function() {
            var go_to_location = $(this).children('.bottom').children('a').attr('href');
            window.location.href = go_to_location;
        });
    });
</script>