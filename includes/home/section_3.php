<?php



$section_three_title;

$section_three_more;

$academic__;

$integrative__;

$alternative__;

$experiences__;

$featured__;


switch ($lang) {
    case "en":

        $section_three_title = "Treatment categories and our featured and newest articles on them...";

        $section_three_more = "More";

        $academic__ = "Academic";

        $integrative__ = "Integrative";

        $alternative__ = "Alternative";

        $experiences__ = "Experiences";

        $featured__ = "Featured";
        break;
    case "sv":
        $section_three_title = "Behandlingskategorier och våra presenterade och nyaste artiklar om dem...";

        $section_three_more = "Mera";

        $academic__ = "Akademisk";

        $integrative__ = "Integrativ";

        $alternative__ = "Alternativ";

        $experiences__ = "Erfarenheter";

        $featured__ = "Rekommenderad";
        break;
    case "fi":
        $section_three_title = "Hoitokategoriat ja niiden tarjoamat ja uusimmat artikkelit...";

        $section_three_more = "Lisää";

        $academic__ = "Akateeminen";

        $integrative__ = "Integratiivinen";

        $alternative__ = "Vaihtoehtoinen";

        $experiences__ = "Kokemuksia";

        $featured__ = "Suositeltu";
        break;
    default:

        $section_three_title = "Treatment categories and our featured and newest articles on them...";

        $section_three_more = "More";

        $academic__ = "Academic";

        $integrative__ = "Integrative";

        $alternative__ = "Alternative";

        $experiences__ = "Experiences";

        $featured__ = "Featured";
}

?>

    <h2 class="title"><?php echo $section_three_title; ?></h2>


    <div class='some-page-wrapper'>

        <div class='row'>

            <div class='column'>

                <div id="column_aca" class='academic-column'>

                    <h2 class="category_title"><?php echo $academic__; ?></h2>

                    <?php

                    $query = "SELECT * FROM entries WHERE (entry_category LIKE '%Academic%') ORDER BY isFeatured DESC, entry_date DESC LIMIT 4;";

                    include "includes/query.php"; ?>

                    <a class="categories_home" href="pages/categories.php?lang=<?php echo $lang; ?>&category=academic"><?php echo $section_three_more; ?></a>

                </div>

            </div>



            <div class='column'>

                <div id="column_int" class='integrative-column'>

                    <h2 class="category_title"><?php echo $integrative__; ?></h2>

                    <?php

                    $query = "SELECT * FROM entries WHERE (entry_category LIKE '%Integrative%') ORDER BY isFeatured DESC, entry_date DESC LIMIT 4;";

                    include "includes/query.php"; ?>

                    <a class="categories_home" href="pages/categories.php?lang=<?php echo $lang; ?>&category=integrative"><?php echo $section_three_more; ?></a>
                </div>

            </div>

        </div>

        <div class="row">

            <div class='column'>

                <div id="column_alt" class='alternative-column'>



                    <h2 class="category_title"><?php echo $alternative__; ?></h2>

                    <?php

                    $query = "SELECT * FROM entries WHERE (entry_category LIKE '%Alternative%') ORDER BY isFeatured DESC, entry_date DESC LIMIT 4;";

                    include "includes/query.php"; ?>

                    <a class="categories_home" href="pages/categories.php?lang=<?php echo $lang; ?>&category=alternative"><?php echo $section_three_more; ?></a>
                </div>

            </div>



            <div class='column'>

                <div id="column_exp" class='experiences-column'>



                    <h2 class="category_title"><?php echo $experiences__; ?></h2>

                    <?php

                    $query = "SELECT * FROM entries WHERE (entry_category LIKE '%Experiences%') ORDER BY isFeatured DESC, entry_date DESC LIMIT 4;";

                    include "includes/query.php"; ?>

                    <a class="categories_home" href="pages/categories.php?lang=<?php echo $lang; ?>&category=experiences"><?php echo $section_three_more; ?></a>
                </div>

            </div>

        </div>

    </div>

    <script>
        var url_string = window.location.href;
        var url = new URL(url_string);

        let imagesArray = document.querySelectorAll(".img, .img_normal");

        let titlesArray = document.querySelectorAll(".category_title");

        var c = "<?php echo $_GET['lang']; ?>";



        imagesArray.forEach(function(elem) {

            elem.addEventListener("click", function() {

                var id = elem.id;

                var new_id = id.substr(4, id.length);

                window.location.href = "/pages/post.php?lang=" + c + "&entry_id=" + new_id;

            });

        });

        titlesArray.forEach(function(elem) {

            elem.addEventListener("click", function() {

                var str = elem.innerHTML;

                var category_title = str.toLowerCase();

                switch(category_title) {

                    // Academic
                    case "akateeminen":
                        category_title = "academic"
                        break;
                    case "akademisk":
                        category_title = "academic"
                        break;
                    case "academic":
                        category_title = "academic"
                        break;
                    // Integrative
                    case "integratiivinen":
                        category_title = "integrative"
                        break;
                    case "integrativ":
                        category_title = "integrative"
                        break;
                    case "integrative":
                        category_title = "integrative"
                        break;
                    // Alternative
                    case "vaihtoehtoinen":
                        category_title = "alternative"
                        break;
                    case "alternativ":
                        category_title = "alternative"
                        break;
                    case "alternative":
                        category_title = "alternative"
                        break;
                    
                    // Experiences
                    case "kokemuksia":
                        category_title = "experiences"
                        break;
                    case "erfarenheter":
                        category_title = "experience"
                        break;
                    case "experiences":
                        category_title = "experiences"
                        break;
                    
                }

                window.location.href = "/pages/categories.php?lang=" + c + "&category=" + category_title;

            });

        });
    </script>