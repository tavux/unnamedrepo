<?php

    $title__searchbar;
    $placeholder_searchbar;
    $query_mission_statement;

    switch ($lang) {
        case "en":
            $query_mission_statement = "SELECT * FROM content WHERE title = 'mission_statement' AND language = 'en' LIMIT 1";
            $query_petra_message = "SELECT * FROM content WHERE title = 'petra_message' AND language = 'en' LIMIT 1";
            $title__searchbar = "Search our website:";
            $placeholder_searchbar = "Search for articles...";
            
            break;
        case "sv":
            $query_mission_statement = "SELECT * FROM content WHERE title = 'mission_statement' AND language = 'sv' LIMIT 1 ";
            $query_petra_message = "SELECT * FROM content WHERE title = 'petra_message' AND language = 'sv' LIMIT 1";
            $title__searchbar = "Sök från hemsidan:";
            $placeholder_searchbar = "Sök efter artiklar...";
            break;
        case "fi":
            $query_mission_statement = "SELECT * FROM content WHERE title = 'mission_statement' AND language = 'fi' LIMIT 1 ";
            $query_petra_message = "SELECT * FROM content WHERE title = 'petra_message' AND language = 'fi' LIMIT 1";
            $title__searchbar = "Hae sivulta:";
            $placeholder_searchbar = "Etsi artikkeleita...";
            break;
        default:
            $query_mission_statement = "SELECT * FROM content WHERE title = 'mission_statement' AND language = 'en' LIMIT 1 ";
            $query_petra_message = "SELECT * FROM content WHERE title = 'petra_message' AND language = 'en' LIMIT 1";
            $title__searchbar = "Search our website:";
            $placeholder_searchbar = "Search for articles...";
            break;
        }

        $statement__mission_db_conn = new Dbh();
        $rows = $statement__mission_db_conn->executeSelect($query_mission_statement);

?>


<!-- Section about Petra -->
<div id="wrapper_about">
    <section>
        <img src="../../res/images/people/petra.jpg" alt="">
        
        <div id="textwrap">
        <?php 
        $rows_ = $statement__mission_db_conn->executeSelect($query_petra_message);
        echo $rows_[0]['content'];
        ?>

        </div>

    </section>
</div>
<!-- Search bar title -->
<p id="search__title"><?php echo $title__searchbar;?></p>



<!-- Questionaire -->
<?php if($questionaireEnabled) {?>
    <section id="section__questionaire">
        <?php require_once $questionairePath; ?>
    </section>
<?php } ?>

<div id="temporary">
    <!-- Search bar wrapper -->
<section id="section__search--wrapper">
    <form action="pages/search.php?lang=<?php echo $lang;?>" method="POST">
        <input type="text" name="search" autocomplete="off" placeholder="<?php echo $placeholder_searchbar;?>"><input
            type="submit" id="search_submit" value="Search" />
    </form>
</section>
<section>
    <p id="section__statement--mission"><?php echo $rows[0]['content'];?></p>
</section>
</div>