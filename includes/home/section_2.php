<?php

$tagMessage;

switch ($lang) {
    case "en":
        $tagMessage = "...or click one of these popular tags";
        break;
    case "sv":
        $tagMessage = "...eller tryck på en av dessa popularä taggarna";
        break;
    case "fi":
        $tagMessage = "...tai klikkaa yhtä seuraavista avainsanoista";
        break;
    default:
        $tagMessage = "...or click one of these popular tags";
        break;
    }
?>



<h2 class="tag_title"><?php echo $tagMessage; ?></h2>

<ul class="search_word_tags">



    <?php

    $tagLang = "tag_title";

    $tagOrder = "t.tag_title";

    if($lang != 'en') {

        $tagLang = "tag_title_".$lang;

        $tagOrder = "t.".$tagLang;

    }

    $query = "SELECT t.*

                FROM tags

                AS t

                RIGHT JOIN entries_tags

                AS et

                ON t.tag_id = et.tag_id

                GROUP BY t.tag_id

                ORDER BY COUNT(et.tag_id) DESC LIMIT 20";

    $dbh = new Dbh();

    $rows = $dbh->executeSelect($query);



    foreach ($rows as $row) {

        $url = "/pages/search.php?lang=";

        $url .= $lang;

        $url .= "&tag=";

        $url .= $row[$tagLang];



        ?>

        <li>

            <a href="<?php echo $url; ?>"><?php echo ucfirst($row[$tagLang])?></a>

        </li> 

        

        <?php } ?>

    

</ul>