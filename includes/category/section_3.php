<?php



switch ($lang) {

    case "en":

        $featured_articles_title = "Featured";
        $read_more = "READ MORE";

        break;

    case "sv":

        $featured_articles_title = "Rekommenderad";
        $read_more = "LÄS MER";
        break;

    case "fi":

        $featured_articles_title = "Suositeltu";
        $read_more = "LUE LISÄÄ";

        break;

    default:
}


    
$imageFormat;

if (strpos($_SERVER['HTTP_ACCEPT'], 'image/webp') !== false) {
    // webp is supported!
    $imageFormat = "webp";
} else {
    $imageFormat = "jpg";
}


?>



    <div class="featured_article">

        <p class="title"><u><?php echo $featured_articles_title; ?></u></p>

        <div class="article_container <?php echo $_GET['category'];
                                        echo "_category"; ?>">





            <?php



            $category = $_GET['category'];
            $category = htmlspecialchars($category);

            $query = "SELECT * FROM entries WHERE entry_category = '" . $category . "' ORDER BY entry_views and entry_date DESC LIMIT 1;";



            $dbh = new Dbh();

            $rows = $dbh->executeSelect($query);



            foreach ($rows as $row) {



                $entry = new Entry();

                $entry->setByRow($row);



                ?>

            <?php } ?>



            <div class="post-content">

                <a href="/pages/post.php?lang=<?php echo $lang; ?>&entry_id=<?php echo $entry->getId(); ?>" class="post-title"><u><?php echo $entry->getTitle($lang); ?></u></a>

                <div id="wrap">



                    <div class="container">

                        <div class="left-div left-text">

                            <img src="/res/images/articles/thumb/<?php echo $imageFormat; ?>/<?php echo $entry->getImgId();?>_thumb.<?php echo $imageFormat; ?>" />


                        </div>

                        <div class="right-div right-text">

                            <p><?php echo $entry->getExcerpt($lang); ?></p>



                        </div>
                    </div>
                </div>
            </div>
        </div>



        <div class="footer <?php echo $_GET['category'];
                            echo "_category"; ?> "><a href="/pages/post.php?lang=<?php echo $lang; ?>&entry_id=<?php echo $entry->getId(); ?>">[ <?php echo $read_more; ?> ]</a>

        </div>

    </div>