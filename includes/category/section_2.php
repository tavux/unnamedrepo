<?php



$category =  ucfirst($_GET['category']);
$category = htmlspecialchars($category);

$query = "SELECT * FROM entries WHERE entry_category LIKE '%$category%' ORDER BY entry_views DESC LIMIT 3;";



$dbh = new Dbh();

$rows = $dbh->executeSelect($query);





switch($lang) {

    case "en":

        $top_articles_title = "Top articles";

        break;

    case "sv":

        $top_articles_title = "Toppartiklar";

        break;

    case "fi":

        $top_articles_title = "Suosituimmat artikkelimme";

        break;

    default:

}



    
$imageFormat;

if (strpos($_SERVER['HTTP_ACCEPT'], 'image/webp') !== false) {
    // webp is supported!
    $imageFormat = "webp";
} else {
    $imageFormat = "jpg";
}



?>



<p id="top_articles_title"><u><?php echo $top_articles_title; ?></u></p>

<div class='some-page-wrapper'>

    <div class='row'>

    <?php

    foreach ($rows as $row) {

        $entry = new Entry();

        $entry->setByRow($row);

    ?>

    <div class="box_wrapper">

        <div class="column <?php echo $_GET['category']; echo "_category"; ?>">

            <section class="top"

            style="background: linear-gradient(rgba(200,200,200, 0.45), rgba(200,200,200, 0.45)), url('/res/images/articles/thumb/<?php echo $imageFormat; ?>/<?php echo $entry->getImgId();?>_thumb.<?php echo $imageFormat; ?>');background-position:center;background-size:cover;background-repeat:no-repeat;">

        "></section>

            <section class="bottom"> <a href="/pages/post.php?lang=<?php echo $lang;?>&entry_id=<?php echo $entry->getId(); ?>"

                    class="title"><u><?php echo $entry->getTitle($lang); ?></u></a>

                <p class="excerpt"><?php echo $entry->getExcerpt($lang); ?></p>

            </section>

        </div>

</div>

        <?php } ?>

    </div>

</div>

<script>

$('.categories_section_two').children('.some-page-wrapper').children('.row').children('.column').each(function () {

    $(this).on("click", function() {

        var go_to_location_one = $(this).children('.bottom').children('a').attr('href');

        window.location.href = go_to_location_one;

    });

});

</script>
