<div class="inner-page-content">

<?php

$getFoundationMessage;

switch ($lang) {
	case "en":
		$getFoundationMessage = "SELECT * FROM content WHERE title = 'foundation_message' AND language = 'en' ";
		break;
	case "sv":
		$getFoundationMessage = "SELECT * FROM content WHERE title = 'foundation_message' AND language = 'sv' ";
		break;
	case "fi":
		$getFoundationMessage = "SELECT * FROM content WHERE title = 'foundation_message' AND language = 'fi' ";
		break;
	default:
		$getFoundationMessage = "SELECT * FROM content WHERE title = 'foundation_message' AND language = 'en' ";
		break;
	}

	$foundationMessage = new Dbh();
	$rows = $foundationMessage->executeSelect($getFoundationMessage);

	foreach ($rows as $row) {?><p><?php echo $row['content']; } ?></p>

</div>