<?php

$DEFAULT_LANGUAGE = 'en';

if (isset($_GET['lang'])) {


    $lang = $_GET['lang'];



    $background_en = "/res/images/flags/us.jpg";

    $background_sv = "/res/images/flags/swe.png";

    $background_fi = "/res/images/flags/fi.png";



    switch ($lang) {
        case "en":
            $home = "Home";
            $theFoundation = "The Foundation";
            $donations = "Donations";
            $news = "News";
            $search = "Search";
            break;
        case "sv":
            $home = "Hem";
            $theFoundation = "Stiftelsen";
            $donations = "Donationer";
            $news = "Nyheter";
            $search = "Sök";
            break;
        case "fi":
            $home = "Etusivu";
            $theFoundation = "Säätiö";
            $donations = "Lahjoitukset";
            $news = "Uutiset";
            $search = "Etsi";
            break;
        default:
            $urlRedirect = $_SERVER['PHP_SELF'] . "?" . "lang=" . $DEFAULT_LANGUAGE;
            header("Location: $urlRedirect");
            exit();
    }
} else {
    $urlRedirect = $_SERVER['PHP_SELF'] . "?" . "lang=" . $DEFAULT_LANGUAGE;
    header("Location: $urlRedirect");
    exit();
}

?>

    <!DOCTYPE HTML>

    <html>



    <head>

        <meta charset="utf-8" />
        <meta name="keywords" content="academic, integrative, alternative, experiences, Cancer, Life, Medicine, Cancer treatment, Food, Stress, Nutrition, Studies, Health, Vitamins, Vitamin d, Vegetables, Healing, Mental well-being, Meditation, Conventional medicine, Immune system, support, therapy, ketogenic diet">
        <meta name="description" content="Our mission is to support You in your fight against cancer, and to give you hope by showing you new ways when the traditional medicine cannot help You anymore.">
        <meta name="author" content="Petra Foundation">

        <meta name="viewport" content="height=device-height, 

                      width=device-width, initial-scale=1.0, 

                      minimum-scale=1.0, maximum-scale=1.0, 

                      user-scalable=no">

        <meta http-equiv="X-UA-Compatible" content="ie=edge">


        <title>PetraFoundation - Fight your cancer with holistic cancer care</title>

        <link rel="preload" href="/res/css/header.css" as="style">
        <link rel="preload" href="/res/css/main.css" as="style" />
        <link rel="preload" href="/res/css/single.css" as="style" />


        <link rel="stylesheet" href="/res/css/header.css" />
        <link rel="stylesheet" href="/res/css/main.css" />
        <link rel="stylesheet" href="/res/css/single.css" />

        <link rel="shortcut icon" type="image/png" href="/res/images/logo/favicon.png" />

        <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
        <script type="application/ld+json">
            {
                "@context": "https://schema.org",
                "@type": "WebSite",
                "url": "http://www.petrafoundation.com/",
                "potentialAction": {
                    "@type": "SearchAction",
                    "target": "http://petrafoundation.com/pages/page_search.php?lang=en&Search={search_term_string}",
                    "query-input": "required name=search_term_string"
                }
            }
        </script>


        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-147287513-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];

            function gtag() {
                dataLayer.push(arguments);
            }
            gtag('js', new Date());

            gtag('config', 'UA-147287513-1');
        </script>


    </head>



    <body>

        <div id="langSelector">

            <?php


            $lang_ = $_GET;



            $lang_['lang'] = 'sv';

            $url_sv = http_build_query($lang_);



            $lang_['lang'] = 'fi';

            $url_fi = http_build_query($lang_);



            $lang_['lang'] = 'en';

            $url_en = http_build_query($lang_);





            $enTag = "<a href='" . $_SERVER['PHP_SELF'] . "?" . $url_en . "' style='background-image:url(" . $background_en . ");'>SV</a>";

            $svTag = "<a href='" . $_SERVER['PHP_SELF'] . "?" . $url_sv . "' style='background-image:url(" . $background_sv . ");'>SV</a>";

            $fiTag = "<a href='" . $_SERVER['PHP_SELF'] . "?" . $url_fi . "' style='background-image:url(" . $background_fi . ");'>SV</a>";


            // If page = page_search
            if (basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']) === "page_search.php") {

                $enTag = "<a href='/index.php' style='background-image:url(" . $background_en . ");'>SV</a>";

                $svTag = "<a href='/index.php' style='background-image:url(" . $background_sv . ");'>SV</a>";

                $fiTag = "<a href='/index.php' style='background-image:url(" . $background_fi . ");'>SV</a>";
            }


            switch ($lang) {
                case "en":
                    echo $svTag;
                    echo $fiTag;
                    break;
                case "sv":
                    echo $enTag;
                    echo $fiTag;
                    break;
                case "fi":
                    echo $svTag;
                    echo $enTag;
                    break;
                default:
                    echo $svTag;
                    echo $fiTag;
                    break;
            }
            ?>

        </div>



        <div id="navigation_top_wrapper">



            <ul id="navigation_top">

                <a href="/index.php?lang=<?php echo $lang; ?>"><img src="/res/images/logo/petrafou_logo_011.png" alt=""></a>

                <li class="active"><a href="/index.php?lang=<?php echo $lang; ?>"><?php echo $home; ?></a></li>

                <li><a href="/pages/thefoundation.php?lang=<?php echo $lang; ?>"><?php echo $theFoundation; ?></a>

                </li>

                <li><a href="/pages/donations.php?lang=<?php echo $lang; ?>"><?php echo $donations; ?></a></li>

                <li><a href="https://www.facebook.com/petrafoundation?lang=<?php echo $lang; ?>">Facebook</a></li>

                <li><a href="/pages/news.php?lang=<?php echo $lang; ?>"><?php echo $news; ?></a></li>

                <li id="search__"><a>

                        <form id="search" action="/pages/search.php?lang=<?php echo $lang; ?>" method="POST">

                            <?php echo $search; ?><input autocomplete="off" id="searchbar" name="search" type="text" placeholder="">

                        </form>

                    </a>

                </li>

            </ul>



        </div>

        <script>
            var lang = "<?php echo $lang; ?>";



            if (lang === "sv") {

                document.getElementById("search__").style.maxWidth = "195px";

                document.getElementById("searchbar").style.width = "70%";

            } else if (lang === "fi") {

                document.getElementById("search__").style.maxWidth = "191px";

                document.getElementById("searchbar").style.width = "70%";



            }
        </script>

        <?php require_once $sidenavPath; ?>