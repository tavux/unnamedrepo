<?php



$continue_reading;
$new_;
$imageFormat;

if( strpos( $_SERVER['HTTP_ACCEPT'], 'image/webp' ) !== false ) {
    // webp is supported!
    $imageFormat = "webp";
} else {
    $imageFormat = "jpg";
}



switch($lang) {
    case "en":
        $continue_reading = "Continue Reading...";
        $new_ = "New:";
    break;
    case "sv":
        $continue_reading = "Läs mer...";
        $new_ = "Nya:";
    break;
    case "fi":
        $continue_reading = "Lue lisää...";
        $new_ = "Uudet:";
    break;
}



        $dbh = new Dbh();

        $rows = $dbh->executeSelect($query);



        

        foreach ($rows as $row) {

            $entry = new Entry();

            $entry->setByRow($row);

            

        ?>



<?php if($row['isFeatured'] == 1) { ?>



<!-- FEATURED ARTICLE -->

<section class="featured_post post">

    <p class="featured_title"><?php echo $featured__; ?>:</p>

    <div id="img_<?php echo $entry->getId();?>" class="img"

        style="background: #CACACA url('/res/images/articles/thumb/<?php echo $imageFormat; ?>/<?php echo $entry->getImgId();?>_thumb.<?php echo $imageFormat; ?>');background-position:center;background-size:cover;background-repeat:no-repeat;">

    </div>

    <div class="title_container">

        <h3 class="post_title_featured"><a

                href="/pages/post.php?lang=<?php echo $lang;?>&entry_id=<?php echo $entry->getId(); ?>"><u><?php echo $entry->getTitle($lang); ?></u></a>

        </h3>

    </div>

    <p class="excerpt">

        <?php echo $entry->getExcerpt($lang); ?>

    </p>

</section>

<footer>

    <a class="action"

        href="/pages/post.php?lang=<?php echo $lang;?>&entry_id=<?php echo $entry->getId(); ?>"><?php echo $continue_reading;?></a>

</footer>

<div class="whitePart">

    <p class="<?php echo $entry->getCategory(); ?>_p"><?php echo $new_;?></p>

    <?php } else { ?>

    <!-- Structure of article -->

    <section class="normal_post post">

        <div id="img_<?php echo $entry->getId();?>" class="img_normal"

            style="background: #CACACA url('/res/images/articles/thumb/<?php echo $imageFormat; ?>/<?php echo $entry->getImgId();?>_thumb.<?php echo $imageFormat; ?>');background-position:center;background-size:cover;background-repeat:no-repeat;">

        </div>

        <div class="title_container">

            <div class="post_title <?php echo $entry->getCategory(); ?>">

                <a href="/pages/post.php?lang=<?php echo $lang;?>&entry_id=<?php echo $entry->getId(); ?>"><?php echo $entry->getTitle($lang); ?></a>

            </div>

        </div>

    </section>

    <?php }} ?>

</div>