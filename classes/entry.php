<?php 

class Entry {

    public $id;

    public $image_id;

    public $date;

    public $author;

    public $category;

    private $title;

    private $title_sv;

    private $title_fi;

    private $excerpt;

    private $excerpt_sv;

    private $excerpt_fi;

    private $content;

    private $content_sv;

    private $content_fi;

    private $dbh;

    private $error;

    public function __construct() {

        $servername = "-";
        $dbname = "-";
        $username = "-";
        $password = "-";
    
        $this->dbh = new PDO("mysql:host=$servername;dbname=$dbname;charset=utf8", $username, $password);
    }

    public function createNew( $author, $image_id, $title,$title_sv,$title_fi, $excerpt,$excerpt_sv,$excerpt_fi, $content, $content_sv, $content_fi, $category ) {

        $this->setByParams( -1, `CURRENT_TIMESTAMP`, $author, $image_id, $title,$title_sv,$title_fi, $excerpt,$excerpt_sv,$excerpt_fi, $content, $content_sv, $content_fi, $category);

    }

    



    public function createNewFromPOST( $post ) {

        print_r($post);

        $this->createNew(

            $post['entry_author'],

            $post['entry_image_id'],

            $post['entry_title'],

            $post['entry_title_sv'],

            $post['entry_title_fi'],

            $post['entry_excerpt'],

            $post['entry_excerpt_sv'],

            $post['entry_excerpt_fi'],

            $post['entry_content'],

            $post['entry_content_sv'],

            $post['entry_content_fi'],

            $post['entry_category']

        );

    }

    public function setByParams( $id, $date, $author, $image_id, $title,$title_sv,$title_fi, $excerpt,$excerpt_sv,$excerpt_fi, $content,$content_sv,$content_fi, $category ) {

        if (strlen($author) == 0) {

            $this->id = -1;

        } else {

            $this->id = $id;

            $this->author = $author;

            $this->image_id = $image_id;

            $this->date = $date;

            $this->title = $title;

            $this->title_sv = $title_sv;

            $this->title_fi = $title_fi;

            $this->excerpt = $excerpt;

            $this->excerpt_sv = $excerpt_sv;

            $this->excerpt_fi = $excerpt_fi;

            $this->content = $content;

            $this->content_sv = $content_sv;

            $this->content_fi = $content_fi;

            $this->category = $category;

        }

    }

    public function setByRow( $row ) {

        //print_r($row);

        $this->setByParams (

            $row['entry_id'],

            $row['entry_date'],

            $row['entry_author'],

            $row['entry_image_id'],

            $row['entry_title'],

            $row['entry_title_sv'],

            $row['entry_title_fi'],

            $row['entry_excerpt'],

            $row['entry_excerpt_sv'],

            $row['entry_excerpt_fi'],

            $row['entry_content'],

            $row['entry_content_sv'],

            $row['entry_content_fi'],

            $row['entry_category']

        );

    }

    public function SqlInsertEntry() {

        $this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $query = ' 

            INSERT INTO entries (

                entry_author, entry_image_id, entry_date, entry_excerpt,entry_excerpt_sv,entry_excerpt_fi, entry_title, entry_title_sv, entry_title_fi,

                entry_content,entry_content_sv,entry_content_fi, entry_category)

            VALUES (

                :entry_author, :entry_image_id, :entry_date, :entry_excerpt,:entry_excerpt_sv,:entry_excerpt_fi, :entry_title, :entry_title_sv, :entry_title_fi,

                :entry_content, :entry_content_sv, :entry_content_fi, :entry_category);';

        $stmt = $this->dbh->prepare($query);

        $result = $stmt->execute(array(

            ':entry_author' => $this->author,

            ':entry_image_id' => $this->image_id,

            ':entry_date' => $this->date,

            ':entry_excerpt' => $this->excerpt,

            ':entry_excerpt_sv' => $this->excerpt_sv,

            ':entry_excerpt_fi' => $this->excerpt_fi,

            ':entry_title' => $this->title,

            ':entry_title_sv' => $this->title_sv,

            ':entry_title_fi' => $this->title_fi,

            ':entry_content' => $this->content,

            ':entry_content_sv' => $this->content_sv,

            ':entry_content_fi' => $this->content_fi,

            ':entry_category' => $this->category

        ));

        $this->error = $this->dbh->errorInfo();

        //print_r($this->error);

        $query = '  SELECT entry_id 

                    FROM entries 

                    WHERE entry_author= :entry_author 

                    ORDER BY entry_id 

                    DESC LIMIT 1;';

        $stmt = $this->dbh->prepare($query);

        $stmt->execute(array(

            ':entry_author' => $this->author

        ));

        $this->error = $this->dbh->errorInfo();

        //print_r($this->error);

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        //print_r($row);

        $this->id = $row['entry_id'];

        return $result;

    }

    public function SqlSelectEntryById( $entry_id ) {

        $this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $query = 'SELECT * FROM entries WHERE entry_id= :entry_id;';

        $stmt = $this->dbh->prepare($query);

        $result = $stmt->execute(array(

            ':entry_id' => $entry_id

        ));

        $this->error = $this->dbh->errorInfo();

        $entry = $stmt->fetch(PDO::FETCH_ASSOC);

        $this->setByRow($entry);

        return $result;

    }



    public function updateViews( $entry_id ) {

        $sql = 'UPDATE entries SET entry_views = entry_views + 1 WHERE ( entry_id = :entry_id )';

        $prepStatement = $this->dbh->prepare( $sql );

        $prepStatement->execute(array(':entry_id' => $entry_id));

        

    }

    public function updateTagClicks( $tag_title ) {

        $sql = 'UPDATE tags SET tag_times_clicked = tag_times_clicked + 1 WHERE ( tag_title = :tag_title )';

        $prepStatement = $this->dbh->prepare( $sql );

        $prepStatement->execute(array(':tag_title' => $tag_title));

        

    }

    public function SqlDeleteEntryById( $entry_id ) {

        $sql = 'DELETE FROM entries WHERE ( entry_id = :entry_id )';

        $prepStatement = $this->dbh->prepare( $sql );

        $prepStatement->execute(array(':entry_id' => $entry_id));

    }



    public function SqlUpdateEntryById( $entry_id ) {

        $query = '  UPDATE entries SET 

                    entry_author = :entry_author, 

                    entry_title = :entry_title, 

                    entry_title_sv = :entry_title_sv, 

                    entry_title_fi = :entry_title_fi, 

                    entry_content = :entry_content, 

                    entry_excerpt = :entry_excerpt, 

                    entry_excerpt_sv = :entry_excerpt_sv, 

                    entry_excerpt_fi = :entry_excerpt_fi 

                    WHERE entry_id = :entry_id;';

        $stmt = $this->dbh->prepare($query);

        $result = $stmt->execute(array(

            ':entry_author' => $this->author,

            ':entry_date' => $this->date,

            ':entry_excerpt' => $this->excerpt,

            ':entry_excerpt_sv' => $this->excerpt_sv,

            ':entry_excerpt_fi' => $this->excerpt_fi,

            ':entry_title' => $this->title,

            ':entry_title_sv' => $this->title_sv,

            ':entry_title_fi' => $this->title_fi,

            ':entry_content' => $this->content

        ));

        return $result;

    }

    /**

     * Get the value of id

     */ 

    public function getId()

    {

        return $this->id;

    }

    public function getImgId()

    {

        return $this->image_id;

    }

    /**

     * Set the value of id

     *

     * @return  self

     */ 

    public function setId($id)

    {

        $this->id = $id;

        return $this;

    }

    /**

     * Get the value of date

     */ 

    public function getDate()

    {

        return $this->date;

    }

    /**

     * Set the value of date

     *

     * @return  self

     */ 

    public function setDate($date)

    {

        $this->date = $date;

        return $this;

    }

    /**

     * Get the value of author

     */ 

    public function getAuthor()

    {

        return $this->author;

    }

    /**

     * Set the value of author

     *

     * @return  self

     */ 

    public function setAuthor($author)

    {

        $this->author = $author;

        return $this;

    }

    /**

     * Get the value of category

     */ 

    public function getCategory()

    {

        return $this->category;

    }

    /**

     * Set the value of category

     *

     * @return  self

     */ 

    public function setCategory($category)

    {

        $this->category = $category;

        return $this;

    }











    /**

     * Get the value of title

     */ 

     public function getTitle($lang)

    {

        if($lang === 'en') {

            return $this->title;

        } else if($lang === 'sv') {

            return $this->title_sv;            

        } else if($lang === 'fi') {

            return $this->title_fi;            

        }

    }

    /**

     * Set the value of title

     *

     * @return  self

     */ 

    public function setTitle($title)

    {

        $this->title = $title;

        return $this;

    }

    /**

     * Get the value of excerpt

     */ 

    public function getExcerpt($lang)

    {

        switch ($lang) {

            case "en":

                return $this->excerpt;

                break;

            case "sv":

                return $this->excerpt_sv;

                break;

            case "fi":

                return $this->excerpt_fi;

                break;

            default:

                return $this->excerpt_fi;

                break;

        }

    }

    /**

     * Set the value of excerpt

     *

     * @return  self

     */ 

    public function setExcerpt($excerpt)

    {

        $this->excerpt = $excerpt;

        return $this;

    }

    /**

     * Get the value of content

     */ 

    public function getContent($lang)

    {

        if($lang === 'en') {

            return $this->content;

        } else if($lang === 'sv') {

            return $this->content_sv;            

        } else if($lang === 'fi') {

            return $this->content_fi;            

        }

    }

    /**

     * Set the value of content

     *

     * @return  self

     */ 

    public function setContent($content)

    {

        $this->content = $content;

        return $this;

    }

}

?>