<?php 

class Dbh {

    private $dbh;

    private $error;


    public function __construct() {

        $servername = "POISTETTU";
        $dbname = "POISTETTU";
        $username = "POISTETTU";
        $password = "POISTETTU";
    
        $this->dbh = new PDO("mysql:host=$servername;dbname=$dbname;charset=utf8", $username, $password);
    }

    public function executeQuery($query) {

        $this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $stmt = $this->dbh->prepare($query);

        $result = $stmt->execute();

        $this->error = $this->dbh->errorInfo();

    }

    public function executeSelect($query) {

        $this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $stmt = $this->dbh->prepare($query);

        $result = $stmt->execute();

        $this->error = $this->dbh->errorInfo();

        //print_r($this->error);

        $entry = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //print_r($entry);

        return $entry;

    }

}

?>