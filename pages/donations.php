<?php 

define('ROOTPATH_', realpath($_SERVER["DOCUMENT_ROOT"]));

require_once ROOTPATH_.'/res/config.php';



require_once $entryPath;

require_once $dbhPath;

require_once $headerPath;

switch($lang) {
    case "en":
    $donations_title = "Donations";
    break;
    case "sv":
    $donations_title = "Donationer";
    break;
    case "fi":
    $donations_title = "Lajoitukset";
    break;
}

?>

<div id="page__donations">

<div id="page_donations_wrapper">

    <h1 id="donations_title"><u><?php echo $donations_title; ?></u></h1>
<div id="page_donations">


    <div id="img_overlay">

        <div id="img"></div>

        <div class="title">DONATIONS</div>

    </div>



    <div id="wrapper_content">

        <p>

            Petrafoundation is a non profit organisation

            that is financed by private donations only. We are very grateful even for small donations.

            <br>

            Please instruct your bank to forward payment orders through:

        </p>

        <div id="table_wrap">

        <table>

            <tr id="header">

                  <td>Bank</th>

                  <td>Swift/Bic</th>

                  <td>IBAN</th>

                  <td>In favour of</th>

                  <td>Address</th>

            </tr>

            <tr>

                  <td>Aktia Bank plc, Helsinki, Finland</td>

                  <td>HELSFIHH</td>

                  <td>FI7540551020224543</td>

                  <td>Petra Flanders stiftelse</td>

                  <td>Kvarnbergsgatan 15, 06100 Borga FINLAND</td>

            </tr>



        </table>

        </div>

        <p>

            Thank you for your donation and we wish you Health and Happiness.<br>

            <i>Petrafoundation</i>

        </p>

    </div>

    </div>

</div>

</div>



<!--  

<div class="donate_wrapper">



<div id="notNews">

      <div id="header"></div>

      <div id="message"></div>



      <?php 

include '../includes/donationMessage.php';

?>      </div>

</div>

-->



<?php require_once '../includes/footer.php'; ?>