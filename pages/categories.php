<?php 

define('ROOTPATH_', realpath($_SERVER["DOCUMENT_ROOT"]));

require_once ROOTPATH_.'/res/config.php';



require_once $entryPath;

require_once $dbhPath;

require_once $headerPath;

require_once $sidenavPath;

?>



<script>

document.title = "<?php echo htmlspecialchars(ucfirst($_GET['category'])) . " - Petrafoundation";?>"

</script>

    <div id="fb-root"></div>

    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.2">

    </script>

<!-- SECTION 1 -->

<div id="page__categories">

<div class="categories_section_one <?php echo $_GET['category']; echo "_category"; ?>"><?php require_once "../includes/category/section_1.php" ?></div>



<!-- SECTION 2 -->

<div class="categories_section_two"><?php require_once "../includes/category/section_2.php" ?></div>



<!-- SECTION 3 -->

<div class="categories_section_three"><?php require_once "../includes/category/section_3.php" ?></div>





<!-- SECTION 4 -->

<div class="categories_section_four"><?php require_once "../includes/category/section_4.php" ?></div>



<!-- SECTION 5 -->

<div class="categories_section_five"><?php require_once "../includes/category/section_5.php" ?></div>



<!-- SECTION 6 -->

<div class="categories_section_six <?php echo $_GET['category']; echo "_category"; ?>"><?php require_once "../includes/category/section_6.php" ?></div>

</div>

<?php require_once "../includes/footer.php";?>