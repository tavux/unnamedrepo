<?php

define('ROOTPATH_', realpath($_SERVER["DOCUMENT_ROOT"]));

require_once ROOTPATH_.'/res/config.php';



require_once $entryPath;

require_once $dbhPath;

require_once $headerPath;

require_once $sidenavPath;



switch($_GET['lang']) {

    case "en":

        $news_articles_title = "News";

        break;

    case "sv":

        $news_articles_title = "Nyheter";

        break;

    case "fi":

        $news_articles_title = "Uutiset";

        break;

    default:

}


$imageFormat;

if (strpos($_SERVER['HTTP_ACCEPT'], 'image/webp') !== false) {
    // webp is supported!
    $imageFormat = "webp";
} else {
    $imageFormat = "jpg";
}




?>

    <div id="page__news">

        <div id="page_news_wrapper">

                <h1 id="news_articles_title"><u><?php echo $news_articles_title; ?></u></h1>

    <div class="news_wrapper">

<script>



document.title = "News - Petrafoundation";

</script>

        <?php



    $query = "SELECT * FROM entries WHERE `entry_category` LIKE '%news%' ORDER BY `entry_date` DESC LIMIT 20";



    $dbh = new Dbh();

    $rows = $dbh->executeSelect($query);

    $readmore__;



    switch($_GET['lang']) {

        case "en":

            $readmore__ = "READ MORE";

            break;

        case "sv":

            $readmore__ = "LÄS MER";

            break;

        case "fi":

            $readmore__ = "LUE LISÄÄ";

            break;

        default:

        $readmore__ = "READ MORE";

    }

    



    foreach ($rows as $row) {

        $entry = new Entry();

        $entry->setByRow($row);

        ?>





<div class="featured_article">

            <div class="article_container">

                <div class="post-thumb-wrap">

                        <a class="imgTitle" href="/pages/post.php?lang=<?php echo $lang; ?>&entry_id=<?php echo $entry->getId(); ?>"><?php echo $entry->getTitle($lang);?></a>

                <div class="post-thumb"

                        style="background: linear-gradient(rgba(200,200,200, 0.45), rgba(200,200,200, 0.45)), url('/res/images/articles/thumb/<?php echo $imageFormat; ?>/<?php echo $entry->getImgId();?>_thumb.<?php echo $imageFormat; ?>');background-position:center;background-size:cover;background-repeat:no-repeat;">

                    </div>

                    <div style="margin-top:10px;height:6px;width:100%;" class="<?php echo $entry->getCategory($lang);?>"></div>

                </div>

                <div class="post-content">



                    <div class="title_container">



                        <a href="post.php?lang=<?php echo $lang; ?>&entry_id=<?php echo $entry->getId(); ?>"

                            class="post-title"><?php echo $entry->getTitle($lang); ?></a>

                        <div class="info_container">

                            <br>

                            <a>By <?php echo $entry->getAuthor();?> posted </a>

                            <a><i><?php echo date('M jS, Y', strtotime($entry->getDate())); ?></i></a>

                            <br>

                            <a><i><?php echo $entry->getCategory($lang);?></i></a>

                        </div>



                    </div>

                    <p><?php echo $entry->getExcerpt($lang); ?></p>

                    <a class="readmore <?php

                    $arr = explode(", " ,$entry->getCategory("en"));

                    echo $arr[0];?>"

                        href="/pages/post.php?lang=<?php echo $lang;?>&entry_id=<?php echo $entry->getId();?>"><?php echo $readmore__; ?></a>

                </div>

            </div>

        </div>

            <hr>

            <?php }?>

        </div>

        </div>

        </div>

        <?php require_once $footerPath; ?>