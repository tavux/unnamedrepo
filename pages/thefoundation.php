<?php

define('ROOTPATH_', realpath($_SERVER["DOCUMENT_ROOT"]));

require_once ROOTPATH_ . '/res/config.php';



require_once $entryPath;

require_once $dbhPath;

require_once $headerPath;

switch ($lang) {
      case "en":
            $foundation_title = "The Foundation";
            break;
      case "sv":
            $foundation_title = "Stiftelsen";
            break;
      case "fi":
            $foundation_title = "Säätiö";
            break;
}

?>



      <div id="page_donations_wrapper">
            <h1 id="foundation_title"><u><?php echo $foundation_title; ?></u></h1>

            <div id="page_donations">



                  <div id="page__theFoundation">

                        <?php
                        include '../includes/thefoundationMessage.php';
                        ?>
                  </div>

            </div>

      </div>







      <?php require_once '../includes/footer.php'; ?>