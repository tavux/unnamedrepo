<?php 

define('ROOTPATH_', realpath($_SERVER["DOCUMENT_ROOT"]));

require_once ROOTPATH_.'/res/config.php';



require_once $entryPath;

require_once $dbhPath;

require_once $headerPath;

require_once $sidenavPath;



    $lang = $_GET['lang'];



	if (!isset($_GET['entry_id'])) {

		header("location:index.php?lang=".$lang);

    }

    $string_ = $_GET['entry_id'];
    $string_ = htmlspecialchars($string_);

    $output_ = preg_replace( '/[^0-9]/', '', $string_ );



    $entry = new Entry();

    $entry->updateViews($output_);

    $entry->SqlSelectEntryById($output_);



    ?>

    <script>

    document.title = "<?php echo $entry->getTitle($lang) . " - Petrafoundation"; ?>";

    </script>



<!-- Main -->



<div class="single_section_one">

    <?php require_once "../includes/single/section_1.php"; ?>

</div>

<div class="single_section_two">

    <?php require_once "../includes/single/section_2.php"; ?>

</div>



<div class="single_section_three">

    <?php require_once "../includes/single/section_3.php"; ?>

</div>



<?php require_once $footerPath; ?>