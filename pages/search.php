<?php

define('ROOTPATH_', realpath($_SERVER["DOCUMENT_ROOT"]));

require_once ROOTPATH_ . '/res/config.php';



require_once $entryPath;

require_once $dbhPath;

require_once $headerPath;

require_once $sidenavPath;




$imageFormat;

if (strpos($_SERVER['HTTP_ACCEPT'], 'image/webp') !== false) {
    // webp is supported!
    $imageFormat = "webp";
} else {
    $imageFormat = "jpg";
}




?>

<div id="page_search_wrapper">

    <div id="search_wrapper">



        <?php



        if (isset($_GET['lang'])) {

            $lang = $_GET['lang'];



            if ($lang === 'en') {

                $search_results = "Search results for: ";

                $placeholder = "Search on the website: ";

                $read_more = "READ MORE";

                $no_results = "NO RESULTS";
            } else if ($lang === 'sv') {

                $search_results = "Resultat för: ";

                $placeholder = "Sök på nätsidan: ";

                $read_more = "LÄS MER";

                $no_results = "INGA RESULTAT";
            } else if ($lang === 'fi') {

                $search_results = "Tulokset haulle: ";

                $placeholder = "Hae verkkosivulta: ";

                $read_more = "LUE LISÄÄ";

                $no_results = "EI HAKUTULOKSIA";
            }
        }



        ?>



        <!-- SEARCH -->

        <?php if (isset($_POST['search'])) { ?>

            <h1><?php echo $search_results . htmlspecialchars($_POST['search']); ?></h1>

            <form action="" method="POST">

                <input type="text" name="search" placeholder="<?php echo $placeholder; ?>" value="">

            </form>



            <?php

                $query = $_POST['search'];



                if (strlen($query) <= 0) {

                    echo "SUP";

                    $query = "aslkdalskdj";
                }

                $tag = $query;

                $query = htmlspecialchars($query);
                $tag = htmlspecialchars($tag);

                $query = "SELECT DISTINCT e.entry_id,

                                                e.entry_date,

                                                e.entry_author,

                                                e.entry_image_id,

                                                e.entry_title,

                                                e.entry_title_sv,

                                                e.entry_title_fi,

                                                e.entry_excerpt,

                                                e.entry_excerpt_sv,

                                                e.entry_excerpt_fi,

                                                e.entry_content,

                                                e.entry_content_sv,

                                                e.entry_content_fi,

                                                e.entry_category

                    FROM tags t

                    INNER JOIN entries_tags et

                    ON t.tag_id = et.tag_id

                    INNER JOIN entries e

                    ON et.entries_id = e.entry_id

                    WHERE (t.tag_title LIKE '%$tag%' OR t.tag_title_sv LIKE '%$tag%' OR t.tag_title_fi LIKE '%$tag%' OR e.entry_title LIKE '%$query%' OR e.entry_title_sv LIKE '%$query%' OR e.entry_title_fi LIKE '%$query%')";





                $dbh = new Dbh();

                $rows = $dbh->executeSelect($query);



                foreach ($rows as $row) {

                    $entry = new Entry();

                    $entry->setByRow($row);





                    ?>



                <hr style="opacity: 0.4;width:97%;margin: 0 auto;margin-top:20px;margin-bottom:20px;">

                <div class="featured_article">

                    <div class="article_container">

                        <div class="post-thumb-wrap">

                            <a class="imgTitle" href="/pages/post.php?lang=<?php echo $lang; ?>&entry_id=<?php echo $entry->getId(); ?>"><?php echo $entry->getTitle($lang); ?></a>

                            <div class="post-thumb" style="background: linear-gradient(rgba(200,200,200, 0.45), rgba(200,200,200, 0.45)), url('/res/images/articles/thumb/<?php echo $imageFormat; ?>/<?php echo $entry->getImgId();?>_thumb.<?php echo $imageFormat; ?>');background-position:center;background-size:cover;background-repeat:no-repeat;">

                            </div>

                            <div style="margin-top:10px;height:6px;width:100%;" class="<?php $arr = explode(", ", $entry->getCategory("en"));
                                                                                                echo $arr[0]; ?>"></div>

                        </div>

                        <div class="post-content">



                            <div class="title_container">



                                <a href="_post.php?lang=<?php echo $lang; ?>&entry_id=<?php echo $entry->getId(); ?>" class="post-title"><?php echo strtoupper($entry->getTitle($lang)); ?></a>

                                <div class="info_container">

                                    <br>

                                    <a>By <?php echo $entry->getAuthor(); ?> posted </a>

                                    <a><i><?php echo date('M jS, Y', strtotime($entry->getDate())); ?></i></a>

                                    <br>

                                    <a><i><?php echo $entry->getCategory($lang); ?></i></a>

                                </div>



                            </div>

                            <p><?php echo $entry->getExcerpt($lang); ?></p>

                            <a class="readmore <?php

                                                        $arr = explode(", ", $entry->getCategory("en"));

                                                        echo $arr[0]; ?>" href="/pages/post.php?lang=<?php echo $lang; ?>&entry_id=<?php echo $entry->getId(); ?>"><?php echo $read_more; ?></a>

                        </div>

                    </div>

                </div>



            <?php }
                if (!$rows) {

                    echo $no_results;
                }
            } else if (isset($_GET['tag'])) {

                $tag = htmlspecialchars($_GET['tag']);

                $entry = new Entry();

                $entry->updateTagClicks($_GET['tag']);

                ?>



            <div class='column'>

                <br>

                <br>





                <?php



                    if (isset($_GET['lang'])) {

                        $lang = $_GET['lang'];



                        if ($lang === 'en') {

                            $search_result = "Articles with tag : ";
                        } else if ($lang === 'sv') {

                            $search_result = "Artiklar med tag: ";
                        } else if ($lang === 'fi') {

                            $search_result = "Artikkelit joissa avainsana : ";
                        }
                    }



                    ?>



                <h1><?php echo $search_result . ucfirst($tag); ?></h1>

                <br>

                <br>

                <hr>

                <?php

                $tag = htmlspecialchars($tag);


                    $query = "SELECT DISTINCT e.entry_id,
e.entry_date,
e.entry_author,
e.entry_image_id,
e.entry_title,
e.entry_title_sv,
e.entry_title_fi,
e.entry_excerpt,
e.entry_excerpt_sv,
e.entry_excerpt_fi,
e.entry_content,
e.entry_content_sv,
e.entry_content_fi,
e.entry_category
FROM tags t
INNER JOIN entries_tags et
ON t.tag_id = et.tag_id
INNER JOIN entries e
ON et.entries_id = e.entry_id
WHERE (t.tag_title LIKE '%$tag%' OR t.tag_title_sv LIKE '%$tag%' OR t.tag_title_fi LIKE '%$tag%')";




                    
                    $dbh = new Dbh();

                    $rows = $dbh->executeSelect($query);





                    foreach ($rows as $row) {

                        $entry = new Entry();

                        $entry->setByRow($row);

                        ?>







                    <div class="featured_article">

                        <div class="article_container">

                            <div class="post-thumb-wrap">

                                <a class="imgTitle" href="/pages/post.php?lang=<?php echo $lang; ?>&entry_id=<?php echo $entry->getId(); ?>"><?php echo $entry->getTitle($lang); ?></a>

                                <div class="post-thumb" style="background: linear-gradient(rgba(200,200,200, 0.45), rgba(200,200,200, 0.45)), url('/res/images/articles/thumb/<?php echo $imageFormat; ?>/<?php echo $entry->getImgId();?>_thumb.<?php echo $imageFormat; ?>');background-position:center;background-size:cover;background-repeat:no-repeat;">

                                </div>

                                <div style="margin-top:10px;height:6px;width:100%;" class="<?php echo $entry->getCategory($lang); ?>"></div>

                            </div>

                            <div class="post-content">



                                <div class="title_container">



                                    <a href="post.php?lang=<?php echo $lang; ?>&entry_id=<?php echo $entry->getId(); ?>" class="post-title"><?php echo strtoupper($entry->getTitle($lang)); ?></a>

                                    <div class="info_container">

                                        <br>

                                        <a>By <?php echo $entry->getAuthor(); ?> posted </a>

                                        <a><i><?php echo date('M jS, Y', strtotime($entry->getDate())); ?></i></a>

                                        <br>

                                        <a><i><?php echo $entry->getCategory($lang); ?></i></a>

                                    </div>



                                </div>

                                <p><?php echo $entry->getExcerpt($lang); ?></p>

                                <a class="readmore <?php

                                                            $arr = explode(", ", $entry->getCategory("en"));

                                                            echo $arr[0]; ?>" href="/pages/post.php?lang=<?php echo $lang; ?>&entry_id=<?php echo $entry->getId(); ?>">READ

                                    MORE</a>

                            </div>

                        </div>

                    </div>



                    <hr>

                <?php }

                    if (!$rows) {

                        echo "No articles with this tag";
                    }
                } else if (isset($_GET['category'])) {



                    ?>



                <?php



                    $query = $_GET['category'];

                    $query = htmlspecialchars($query);

                    $query = "SELECT * FROM entries WHERE entry_category LIKE '%" . $query . "%'";



                    $dbh = new Dbh();

                    $rows = $dbh->executeSelect($query);



                    if (!$rows) {

                        echo "<h1>No posts here, sorry :(</h1>";
                    } else {

                        echo "<h1>Category:" . strtoupper($_GET['category']) . "</h1>";





                        foreach ($rows as $row) {

                            $entry = new Entry();

                            $entry->setByRow($row);



                            ?>



                        <hr style="opacity: 0.4;margin-top:30px;">

                        <div class="featured_article">

                            <div class="article_container">

                                <div class="post-thumb-wrap">

                                    <a class="imgTitle" href="/pages/post.php?lang=<?php echo $lang; ?>&entry_id=<?php echo $entry->getId(); ?>"><?php echo $entry->getTitle($lang); ?></a>

                                    <div class="post-thumb" style="background: linear-gradient(rgba(200,200,200, 0.45), rgba(200,200,200, 0.45)), url('/res/images/articles/thumb/<?php echo $imageFormat; ?>/<?php echo $entry->getImgId();?>_thumb.<?php echo $imageFormat; ?>');background-position:center;background-size:cover;background-repeat:no-repeat;">

                                    </div>

                                    <div style="margin-top:10px;height:6px;width:100%;" class="<?php echo $entry->getCategory($lang); ?>"></div>

                                </div>

                                <div class="post-content">



                                    <div class="title_container">



                                        <a href="post.php?lang=<?php echo $lang; ?>&entry_id=<?php echo $entry->getId(); ?>" class="post-title"><?php echo strtoupper($entry->getTitle($lang)); ?></a>

                                        <div class="info_container">

                                            <br>

                                            <a>By <?php echo $entry->getAuthor(); ?> posted </a>

                                            <a><i><?php echo date('M jS, Y', strtotime($entry->getDate())); ?></i></a>

                                            <br>

                                            <a><i><?php echo $entry->getCategory($lang); ?></i></a>

                                        </div>



                                    </div>

                                    <p><?php echo $entry->getExcerpt($lang); ?></p>

                                    <a class="readmore <?php

                                                                    $arr = explode(", ", $entry->getCategory("en"));

                                                                    echo $arr[0]; ?>" href="/pages/post.php?lang=<?php echo $lang; ?>&entry_id=<?php echo $entry->getId(); ?>">READ

                                        MORE</a>

                                </div>

                            </div>

                        </div>

            <?php }}} else if(isset($_GET['Search'])) { ?>
                
                <h1><?php echo $search_results . htmlspecialchars($_GET['Search']); ?></h1>
                <form action="" method="POST">
                    <input type="text" name="search" placeholder="<?php echo $placeholder; ?>" value="">
                </form>

                <?php
                    $query = $_GET['Search'];

                    if (strlen($query) <= 0) { $query = "aslkdalskdj"; }

                    $tag = $query;

                    $query = htmlspecialchars($query);
                    $tag = htmlspecialchars($tag);


                    $query = "SELECT DISTINCT e.entry_id,

                                    e.entry_date,

                                    e.entry_author,

                                    e.entry_image_id,

                                    e.entry_title,

                                    e.entry_title_sv,

                                    e.entry_title_fi,

                                    e.entry_excerpt,

                                    e.entry_excerpt_sv,

                                    e.entry_excerpt_fi,

                                    e.entry_content,

                                    e.entry_content_sv,

                                    e.entry_content_fi,

                                    e.entry_category

                                    FROM tags t

                                    INNER JOIN entries_tags et

                                    ON t.tag_id = et.tag_id

                                    INNER JOIN entries e

                                    ON et.entries_id = e.entry_id

                                    WHERE (t.tag_title LIKE '%$tag%' OR t.tag_title_sv LIKE '%$tag%' OR t.tag_title_fi LIKE '%$tag%' OR e.entry_title LIKE '%$query%' OR e.entry_title_sv LIKE '%$query%' OR e.entry_title_fi LIKE '%$query%')";

                                    $dbh = new Dbh();

    $rows = $dbh->executeSelect($query);



    foreach ($rows as $row) {
        $entry = new Entry();
        $entry->setByRow($row);
        ?>



    <hr style="opacity: 0.4;width:97%;margin: 0 auto;margin-top:20px;margin-bottom:20px;">

    <div class="featured_article">

        <div class="article_container">

            <div class="post-thumb-wrap">

                <a class="imgTitle" href="/pages/post.php?lang=<?php echo $lang; ?>&entry_id=<?php echo $entry->getId(); ?>"><?php echo $entry->getTitle($lang); ?></a>

                <div class="post-thumb" style="background: linear-gradient(rgba(200,200,200, 0.45), rgba(200,200,200, 0.45)), url('/res/images/articles/thumb/<?php echo $imageFormat; ?>/<?php echo $entry->getImgId();?>_thumb.<?php echo $imageFormat; ?>');background-position:center;background-size:cover;background-repeat:no-repeat;">

                </div>

                <div style="margin-top:10px;height:6px;width:100%;" class="<?php $arr = explode(", ", $entry->getCategory("en"));
                                                                                    echo $arr[0]; ?>"></div>

            </div>

            <div class="post-content">



                <div class="title_container">



                    <a href="post.php?lang=<?php echo $lang; ?>&entry_id=<?php echo $entry->getId(); ?>" class="post-title"><?php echo strtoupper($entry->getTitle($lang)); ?></a>

                    <div class="info_container">

                        <br>

                        <a>By <?php echo $entry->getAuthor(); ?> posted </a>

                        <a><i><?php echo date('M jS, Y', strtotime($entry->getDate())); ?></i></a>

                        <br>

                        <a><i><?php echo $entry->getCategory($lang); ?></i></a>

                    </div>
                </div>

                <p><?php echo $entry->getExcerpt($lang); ?></p>

                <a class="readmore <?php

                                            $arr = explode(", ", $entry->getCategory("en"));

                                            echo $arr[0]; ?>" href="/pages/post.php?lang=<?php echo $lang; ?>&entry_id=<?php echo $entry->getId(); ?>"><?php echo $read_more; ?></a>

            </div>
        </div>
    </div>



<?php }} ?>

            </div>

    </div>

</div>

<?php require_once "../includes/footer.php"; ?>