<?php
$questionaireTitle;

if($lang === 'en') {
  $questionaireTitle = "FIND ARTICLES FOR <i>YOU</i>";  
} else if($lang === 'sv') {
  $questionaireTitle = "HITTA ARTIKLAR FÖR <i>DIG</i>";  
} else if($lang === 'fi') {
  $questionaireTitle = "LÖYDÄ ARTIKKELEITA <i>SINULLE</i>";  
}

?>

<h2 class="title"><?php echo $questionaireTitle;?></h2>
<div class="quiz-container">
  <div id="quiz"></div>
</div>

<button id="submit_btn">Finish</button>
<button id="next">DISABLED</button>
<button id="previous">Previous</button>


<script src="<?php echo $questionaireJsPath; ?>"></script>