var questionNr;

(function () {

    const myQuestions = [
        {
            question: "What is your situation?",
            answers: {
                a: "I just received my cancer diagnosis",
                b: "I've been told I can't be cured",
                c: "I have no diagnosis, but am worried about cancer",
                d: "I'm receiving treatment currently"
            }
        },
        {
            question: "What is your situation?",
            answers: {
                a: "I just received my cancer diagnosis",
                b: "I've been told I can't be cured",
                c: "I have no diagnosis, but am worried about cancer",
                d: "I'm receiving treatment currently"
            }
        },
        {
            question: "What is your situation?",
            answers: {
                a: "I just received my cancer diagnosis",
                b: "I've been told I can't be cured",
                c: "I have no diagnosis, but am worried about cancer",
                d: "I'm receiving treatment currently"
            }
        },
        {
            question: "What is your situation?",
            answers: {
                a: "I just received my cancer diagnosis",
                b: "I've been told I can't be cured",
                c: "I have no diagnosis, but am worried about cancer",
                d: "I'm receiving treatment currently"
            }
        },
        {
            question: "What is your situation?",
            answers: {
                a: "I just received my cancer diagnosis",
                b: "I've been told I can't be cured",
                c: "I have no diagnosis, but am worried about cancer",
                d: "I'm receiving treatment currently"
            }
        },
        {
            question: "What is your situation?",
            answers: {
                a: "I just received my cancer diagnosis",
                b: "I've been told I can't be cured",
                c: "I have no diagnosis, but am worried about cancer",
                d: "I'm receiving treatment currently"
            }
        },
        {
            question: "What is your situation?",
            answers: {
                a: "I just received my cancer diagnosis",
                b: "I've been told I can't be cured",
                c: "I have no diagnosis, but am worried about cancer",
                d: "I'm receiving treatment currently"
            }
        },
        {
            question: "What is your situation?",
            answers: {
                a: "I just received my cancer diagnosis",
                b: "I've been told I can't be cured",
                c: "I have no diagnosis, but am worried about cancer",
                d: "I'm receiving treatment currently"
            }
        },
        {
            question: "What is your situation?",
            answers: {
                a: "I just received my cancer diagnosis",
                b: "I've been told I can't be cured",
                c: "I have no diagnosis, but am worried about cancer",
                d: "I'm receiving treatment currently"
            }
        },
        {
            question: "What is your situation?",
            answers: {
                a: "I just received my cancer diagnosis",
                b: "I've been told I can't be cured",
                c: "I have no diagnosis, but am worried about cancer",
                d: "I'm receiving treatment currently"
            }
        },
        {
            question: "What is your situation?",
            answers: {
                a: "I just received my cancer diagnosis",
                b: "I've been told I can't be cured",
                c: "I have no diagnosis, but am worried about cancer",
                d: "I'm receiving treatment currently"
            }
        },
        {
            question: "What is your situation?",
            answers: {
                a: "I just received my cancer diagnosis",
                b: "I've been told I can't be cured",
                c: "I have no diagnosis, but am worried about cancer",
                d: "I'm receiving treatment currently"
            }
        }
    ];

    function buildQuiz() {
        questionNr = 1;
        // we'll need a place to store the HTML output
        const output = [];

        // for each question...
        myQuestions.forEach((currentQuestion, questionNumber) => {
            // we'll want to store the list of answer choices
            const answers = [];

            // and for each available answer...
            for (letter in currentQuestion.answers) {
                // ...add an HTML radio button
                answers.push(
                    `<label>
               <input type="radio" name="question${questionNumber}" value="${letter}">
                
                ${currentQuestion.answers[letter]}
             </label>`
                );
            }

            // add this question and its answers to the output
            output.push(
                `<div class="slide">
             <div class="question"><h3> ${currentQuestion.question} </h3></div>
             <div class="answers"> ${answers.join("")} </div>
           </div>`
            );
        });

        // finally combine our output list into one string of HTML and put it on the page
        quizContainer.innerHTML = output.join("");
        quizContainer.innerHTML += (`<div id='question_nr'>Question ${questionNr}/12 </div>`);
    }

    function updateQuestionValue(direction) {
        if (direction == "increment") {
            questionNr = questionNr + 1;
            document.getElementById("question_nr").innerHTML = "Question " + questionNr + "/12";
        } else if (direction == "decrement") {
            questionNr = questionNr - 1;
            document.getElementById("question_nr").innerHTML = "Question " + questionNr + "/12";
        }
    }
    function showResults() {
        // gather answer containers from our quiz
        const answerContainers = quizContainer.querySelectorAll(".answers");

        // keep track of user's answers
        let numCorrect = 0;

        let searchString = null;

        myQuestions.forEach((currentQuestion, questionNumber) => {
            // find selected answer
            const answerContainer = answerContainers[questionNumber];
            const selector = `input[name=question${questionNumber}]:checked`;
            const userAnswer = (answerContainer.querySelector(selector) || {}).value;
            if(searchString === null) {
                searchString = userAnswer;
            } else {
                searchString += userAnswer;
            }
        });
        
        document.location.href="/dist/pages/page_search.php?questionaire="+searchString;


    }

    function showSlide(n) {
        slides[currentSlide].classList.remove("active-slide");
        slides[n].classList.add("active-slide");
        currentSlide = n;

        if (currentSlide === 0) {
            previousButton.style.display = "none";
        } else {
            previousButton.style.display = "inline-block";
        }

        if (currentSlide === slides.length - 1) {
            nextButton.style.display = "none";
            submitButton.style.display = "inline-block";
        } else {
            nextButton.style.display = "inline-block";
            submitButton.style.display = "none";
        }
    }

    function showNextSlide() {
        updateQuestionValue("increment");
        showSlide(currentSlide + 1);
    }

    function showPreviousSlide() {
        updateQuestionValue("decrement");
        showSlide(currentSlide - 1);
    }

    const quizContainer = document.getElementById("quiz");
    const resultsContainer = document.getElementById("results");
    const submitButton = document.getElementById("submit_btn");

    // display quiz right away
    buildQuiz();

    const previousButton = document.getElementById("previous");
    const nextButton = document.getElementById("next");
    const slides = document.querySelectorAll(".slide");
    let currentSlide = 0;

    showSlide(0);

    // on submit, show results
    submitButton.addEventListener("click", showResults);
    previousButton.addEventListener("click", showPreviousSlide);
    nextButton.disabled = true;
    nextButton.addEventListener("click", showNextSlide);
})();