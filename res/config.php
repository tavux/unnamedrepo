<?php

define('ROOTPATH', realpath($_SERVER["DOCUMENT_ROOT"]));

$entryPath = ROOTPATH.'/classes/entry.php';
$dbhPath = ROOTPATH.'/classes/dbh.php';
$headerPath = ROOTPATH.'/includes/header.php';
$sidenavPath = ROOTPATH.'/includes/sidenav.php';
$footerPath = ROOTPATH.'/includes/footer.php';


$questionairePath = ROOTPATH.'/res/includes/questionaire.php';
$questionaireJsPath = '/res/js/questionaire.js';

// Enable Or Disable the questionaire on the homepage
$questionaireEnabled = false;



?>